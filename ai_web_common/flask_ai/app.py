import hashlib
import pkg_resources
from flask import request, session, g
from flask_talisman import DENY
from opentelemetry.instrumentation.flask import FlaskInstrumentor
from .tracing import setup_tracing
from whitenoise import WhiteNoise


DEFAULT_CSP_REPORT_URI = 'https://live-reports.autistici.org/ingest/v1'

DEFAULT_LANGUAGES = [
    ('en', 'English'),
    ('it', 'Italiano'),
]


_telemetry_instrumentor = FlaskInstrumentor()


def init_app(app, talisman):
    """Initialize the Flask application."""

    talisman.init_app(
        app,
        frame_options=DENY,
        referrer_policy='no-referrer',
        content_security_policy={
            'default-src': '\'self\'',
            'img-src': ['\'self\'', 'data:'],
        },
        content_security_policy_report_uri=app.config.get(
            'CSP_REPORT_URI', DEFAULT_CSP_REPORT_URI),
        force_https=not app.config['TESTING'],
    )

    app.wsgi_app = WhiteNoise(
        app.wsgi_app,
        root=pkg_resources.resource_filename(
            app.import_name, 'static'),
        prefix='static/',
        max_age=app.config.get(
            'SEND_FILE_MAX_AGE_DEFAULT', 900),
    )

    if 'SUPPORTED_LANGUAGES' not in app.config:
        app.config['SUPPORTED_LANGUAGES'] = DEFAULT_LANGUAGES
    app.config['SUPPORTED_LANGUAGES_ISO'] = [
        x[0] for x in app.config['SUPPORTED_LANGUAGES']]

    _telemetry_instrumentor.instrument_app(app)
    if setup_tracing(app.import_name):
        app.logger.info('configured request tracing')

    # Autodetect language as best as we can, by retrieving it in order
    # from either:
    #
    # - the session
    #
    # - (the user language, when using @with_user, which gets saved in
    #   the session)
    #
    # - the accept-languages http header sent by the browser
    @app.before_request
    def _autodetect_lang():
        if 'lang' in session:
            g.lang = session['lang']
        else:
            g.lang = request.accept_languages.best_match(
                app.config['SUPPORTED_LANGUAGES_ISO'])
        if not g.lang:
            g.lang = 'en'

    @app.after_request
    def _set_custom_headers(response):
        if not request.path.startswith('/static/'):
            response.headers['Cache-control'] = 'no-store'
            response.headers['Pragma'] = 'no-cache'
            response.headers['Expires'] = '-1'
        return response

    # Generic md5 template filter.
    @app.template_filter('md5sum')
    def md5sum(s):
        return hashlib.md5(s.encode('utf-8')).hexdigest()

    # Function used to return a relative path.
    @app.template_filter('relpath')
    def relpath(s, root):
        if s.startswith(root):
            return s[len(root)+1:]
        return s

    return app
