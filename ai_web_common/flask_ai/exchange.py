import time
from flask import session, g


# Time to live of cached third-party SSO tokens (in seconds).
THIRD_PARTY_SSO_TTL = 120


def sso_exchange(login_service_client, sso_service):
    """Return a sso exchange callable.

    The callable satisfies the sso_exchange interface defined by
    ai_web_common.rpc.core.ClientBase. The provided sso_service must
    be the SSO service of the current application.
    """

    def _sso_exchange(new_service, ignore_cache=False):
        """Exchange the current SSO token for a third-party one.

        Helper function that obtains a third-party SSO token via the
        exchange method of the login service, and stores it for a short
        period of time in the Flask session.

        TODO: this function is currently slightly broken: if the current
        SSO token has a validity left that's less than
        THIRD_PARTY_SSO_TTL, you'll get a token that is valid for
        less than the expected expiration time, which will lead to 403
        errors on backends.

        """
        now = time.time()
        tpc = session.get('tpc', {})
        if not ignore_cache and new_service in tpc:
            expiry, tkt = tpc[new_service]
            if expiry > now:
                return tkt
        tkt = login_service_client.exchange(
            cur_tkt=g.raw_sso_ticket,
            cur_svc=sso_service,
            cur_nonce=session.get('nonce'),
            new_svc=new_service,
        )
        # Subtract a 10-second buffer to the cache TTL.
        tpc[new_service] = (now + THIRD_PARTY_SSO_TTL - 10, tkt)
        # If we don't set the session attribute again, Flask won't notice
        # that it changed.
        session['tpc'] = tpc
        return tkt

    return _sso_exchange
