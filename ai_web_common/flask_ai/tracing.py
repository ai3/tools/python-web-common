import json
import os
import socket

from opentelemetry import trace
from opentelemetry.exporter.zipkin.json import ZipkinExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.resources import Resource, \
    SERVICE_NAME, HOST_NAME, CONTAINER_NAME, CONTAINER_IMAGE_NAME, CONTAINER_IMAGE_TAG


tracing_config_file = os.getenv('TRACING_CONFIG', '/etc/tracing/client.conf')


def setup_tracing(service_name):
    try:
        with open(tracing_config_file) as fd:
            config = json.load(fd)
    except (OSError, IOError):
        return False
    if 'report_url' not in config:
        return False

    default_resource_attributes = {
        SERVICE_NAME: service_name,
        HOST_NAME: socket.gethostname(),
    }
    if os.getenv('FLOAT_SERVICE'):
        default_resource_attributes['float.service'] = os.getenv('FLOAT_SERVICE')
    if os.getenv('FLOAT_INSTANCE_NAME'):
        default_resource_attributes['float.instance'] = os.getenv('FLOAT_INSTANCE_NAME')
    if os.getenv('FLOAT_CONTAINER_NAME'):
        default_resource_attributes[CONTAINER_NAME] = os.getenv('FLOAT_CONTAINER_NAME')
    if os.getenv('FLOAT_CONTAINER_IMAGE'):
        image_name, tag = os.getenv('FLOAT_CONTAINER_IMAGE'), 'latest'
        if ':' in image_name:
            image_name, tag = image_name.split(':', 1)
        default_resource_attributes[CONTAINER_IMAGE_NAME] = image_name
        default_resource_attributes[CONTAINER_IMAGE_TAG] = tag

    resource = Resource(attributes=default_resource_attributes)

    zipkin_exporter = ZipkinExporter(endpoint=config['report_url'])

    provider = TracerProvider(resource=resource)
    processor = BatchSpanProcessor(zipkin_exporter)
    provider.add_span_processor(processor)
    trace.set_tracer_provider(provider)
    return True
