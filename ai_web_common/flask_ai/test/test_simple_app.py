from ai_web_common.flask_ai.app import init_app
from ai_web_common.flask_ai.test import TestCase
from flask import Flask, make_response
from flask_talisman import Talisman


app = Flask(__name__)
talisman = Talisman()


@app.route('/')
def index():
    return make_response('ok')


class TestSimpleApp(TestCase):

    def create_app(self):
        app.config.update({
            'TESTING': True,
            'DEBUG': False,
        })
        init_app(app, talisman)
        return app

    def test_request(self):
        r = self.client.get('/')
        self.assertEqual(200, r.status_code)
        self.assertEqual('ok', r.data.decode('utf-8'))
