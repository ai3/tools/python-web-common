from __future__ import print_function

import base64
import json
import optparse
import os
import sys
from hashlib import sha384


def compute_checksum(path):
    with open(path, 'rb') as fd:
        data = fd.read()
        h = sha384(data).digest()
        b = base64.b64encode(h)
        return str('sha384-' + b.decode().strip())


def main():
    parser = optparse.OptionParser()
    parser.add_option('--python', action='store_true',
                      help='Generate Python code instead of JSON')
    parser.add_option('--base', default='/static',
                      help='Path prefix of public URLs')
    parser.add_option('--output',
                      help='Output path (default: stdout)')
    opts, args = parser.parse_args()

    dir = '.'
    if args:
        dir = args[0]
    if dir.endswith('/'):
        dir = dir[:-1]

    base_url = opts.base
    if not base_url.endswith('/'):
        base_url += '/'

    sri_map = {}

    for root, dirs, files in os.walk(dir):
        for f in files:
            path = os.path.join(root, f)
            rel_path = path[len(dir)+1:]
            url = os.path.join(base_url, rel_path)
            sri_map[url] = compute_checksum(path)

    if opts.output:
        outfd = open(opts.output, 'w')
    else:
        outfd = sys.stdout

    if opts.python:
        print("sri_map = %s\n" % (sri_map,), file=outfd)
    else:
        json.dump(sri_map, outfd)


if __name__ == '__main__':
    main()
