import backoff
import codecs
import json
import random
import socket
import ssl
import threading
import time
import urllib3
import urllib3.util

from opentelemetry import trace
from opentelemetry.trace.status import Status
from opentelemetry.semconv.trace import SpanAttributes
from opentelemetry.instrumentation.utils import http_status_to_status_code


DNS_CACHE_TTL = 60

DEFAULT_REQUEST_TIMEOUT = 10
DEFAULT_MAX_TIMEOUT = 30
DEFAULT_MAX_BACKOFF_INTERVAL = 3


class StatusError(Exception):

    def __init__(self, url, resp, extra_msg=None):
        self.url = url.url
        self.code = resp.status
        msg = '%s: HTTP error %d: %s' % (self.url, self.code, resp.reason)
        if extra_msg:
            msg += ' '
            msg += extra_msg
        super(StatusError, self).__init__(msg)


class RetriableStatusError(StatusError):
    pass


class BadRequestError(StatusError):

    def __init__(self, url, resp):
        # Some backends return structured 400 errors with a
        # JSON-encoded body containing field-level validation
        # errors. Detect this and make it available in the 'errors'
        # attribute of the exception object.
        try:
            self.errors = _json_response_decoder(resp)
            msg = json.dumps(self.errors)
        except ValueError:
            self.errors = {}
            msg = None

        super(BadRequestError, self).__init__(url, resp, extra_msg=msg)


class AuthenticationError(StatusError):
    pass


class _DNS():
    """A process-wide cache of DNS entries."""

    TTL = DNS_CACHE_TTL

    def __init__(self):
        self._lock = threading.Lock()
        self._cache = {}

    def resolve(self, name, port):
        with self._lock:
            now = time.time()
            if name in self._cache:
                addrs, exp = self._cache[name]
                if exp > now:
                    return addrs
            addrs = {x[4] for x in socket.getaddrinfo(name, port)}
            self._cache[name] = (addrs, now + self.TTL)
            return addrs


_dns_cache = _DNS()


class _Targets():
    """List of resolved targets for an address.

    This object can be iterated indefinitely by calling next(), entries
    will just repeat regularly.
    """

    def __init__(self, addrs):
        if not addrs:
            raise ValueError('empty target list')
        self._addrs = list(addrs)
        random.shuffle(self._addrs)
        self._pos = -1

    def next(self):
        """Return a target and increment the position."""
        self._pos += 1
        if self._pos >= len(self._addrs):
            self._pos = 0
        return self._addrs[self._pos]

    @classmethod
    def resolve(cls, name, port):
        """Resolve an address and return a _Targets object."""
        return cls(_dns_cache.resolve(name, port))


_pools = {}
_pool_mx = threading.Lock()


def _get_pool(cls, host, port, **kwargs):
    key = f'{host}:{port}'
    with _pool_mx:
        if key in _pools:
            return _pools[key]
        p = cls(host, port, **kwargs)
        _pools[key] = p
        return p


def _ssl_context(cert_path, key_path, ca_path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.verify_flags = ssl.VERIFY_DEFAULT
    context.check_hostname = True
    context.load_verify_locations(ca_path)
    context.load_cert_chain(cert_path, key_path)
    return context


def _json_request_encoder(req):
    return json.dumps(req)


def _json_response_decoder(resp):
    content_type = resp.headers.get('Content-Type')
    if not content_type or not content_type.startswith('application/json'):
        raise ValueError('response is not application/json')

    reader = codecs.getreader('utf-8')
    return json.load(reader(resp))


class ClientStub():

    """Client stub for JSON/RPC services."""

    NAME = None
    METHODS = []

    def __init__(self, backend, sso_exchange=None):
        """Initialize a client stub.

        Takes a dictionary with the backend specification. The
        specification should include the following attributes:

        * url - URL of the backend
        * ssl_cert - SSL certificate for client auth
        * ssl_key - SSL private key for client auth
        * ssl_ca - SSL CA to validate server certificate
        * timeout - timeout for requests, in seconds (default 10)
        * sharded - whether the service is sharded or not
        * sso_service - SSO service used by this backend, if any

        'sso_exchange' is a function that can be called at runtime to
        obtain a SSO token for an arbitrary backend.

        """
        if 'url' not in backend:
            raise ValueError('backend spec does not contain "url"')
        self._url = backend['url'].rstrip('/')
        self._sso_service = backend.get('sso_service')
        self._sso_exchange = sso_exchange

        self._ssl_context = None
        if 'ssl_cert' in backend:
            self._ssl_context = _ssl_context(
                backend['ssl_cert'], backend['ssl_key'], backend['ssl_ca'])

        self._timeout = int(
            backend.get('timeout', DEFAULT_REQUEST_TIMEOUT))

        # Dynamically create function methods on this object by
        # looking at the METHODS class attribute.
        for method, url in self.METHODS:
            if backend.get('sharded'):
                setattr(self, method, self._sharded_method(url))
            else:
                setattr(self, method, self._method(url))

    @backoff.on_exception(backoff.expo,
                          (urllib3.exceptions.HTTPError,
                           RetriableStatusError),
                          base=1.4142,
                          factor=0.1,
                          max_value=DEFAULT_MAX_BACKOFF_INTERVAL,
                          max_time=DEFAULT_MAX_TIMEOUT)
    def _do_single_request(self, parsed_url, targets, request_data,
                           headers, response_decoder, deadline):
        """Issue a request, retry on temporary errors.

        Temporary errors include all HTTP status codes >500, and the
        special status code 429 (throttling). We'll use an exponential
        backoff for retry intervals.

        If the response status is not 200, this function will raise a
        StatusError with the response code and body (which may contain
        a meaningful error message).

        Also, since the opencensus-ext-requests library does not patch
        the requests API that we are using, we have to support tracing
        ourselves.

        """

        # By checking the timestamp now, we can bail out early
        # even if the 'backoff' retry loop is still running (with
        # an approximate correctness of +/- max_value).
        if time.time() > deadline:
            raise TimeoutError()

        target_addr, target_port = targets.next()

        tracer = trace.get_tracer(self.NAME)
        with tracer.start_as_current_span(
                f'[client-rpc] {self.NAME}',
                kind=trace.SpanKind.CLIENT,
                attributes={
                    SpanAttributes.PEER_SERVICE: self.NAME,
                    SpanAttributes.HTTP_URL: parsed_url.url,
                    SpanAttributes.HTTP_HOST: target_addr,
                    SpanAttributes.HTTP_METHOD: 'POST',
                },
        ) as span:

            pool_args = {
                'cls': urllib3.HTTPConnectionPool,
                'host': target_addr,
                'port': target_port,
                'timeout': urllib3.util.Timeout(connect=2, read=5),
                'block': True,
            }
            if parsed_url.scheme == 'https':
                pool_args['cls'] = urllib3.HTTPSConnectionPool
                pool_args['assert_hostname'] = parsed_url.host
                pool_args['server_hostname'] = parsed_url.host
                pool_args['ssl_context'] = self._ssl_context
            pool = _get_pool(**pool_args)

            hdrs = {
                'Host': parsed_url.host,
                'Content-Type': 'application/json',
            }
            if headers:
                hdrs.update(headers)
            try:
                hdrs.update(
                    tracer.propagator.to_headers(tracer.span_context))
            except AttributeError:
                pass

            actual_url = urllib3.util.Url(
                path=parsed_url.path,
                query=parsed_url.query,
            )
            resp = pool.urlopen(
                'POST',
                actual_url.url,
                request_data,
                headers=hdrs,
                assert_same_host=False,
                preload_content=False,
                retries=False,
            )
            try:
                span.set_attribute(SpanAttributes.HTTP_STATUS_CODE, resp.status)
                span.set_status(Status(http_status_to_status_code(resp.status)))

                if resp.status == 429 or resp.status > 500:
                    raise RetriableStatusError(parsed_url, resp)
                elif resp.status == 400:
                    raise BadRequestError(parsed_url, resp)
                elif resp.status == 403:
                    raise AuthenticationError(parsed_url, resp)
                elif resp.status != 200:
                    raise StatusError(parsed_url, resp)

                return response_decoder(resp)

            finally:
                resp.release_conn()

    def _do_request(self, url, headers=None,
                    override_target_list=None,
                    request_encoder=_json_request_encoder,
                    response_decoder=_json_response_decoder,
                    **kwargs):
        # If the backend requires SSO authentication, add a 'sso'
        # request attribute with a valid ticket. If we get an
        # authentication error, retry by forcing the exchange of a new
        # ticket (presumably the current one has expired).
        parsed_url = urllib3.util.parse_url(url)
        if override_target_list:
            targets = _Targets(override_target_list)
        else:
            targets = _Targets.resolve(parsed_url.host, parsed_url.port)

        # Run an outer loop to satisfy the overall request timeout,
        # calling the backoff inner loop multiple times during the
        # process. We do this because the backoff timeout is set once
        # globally in the decorator, and we can't influence it
        # per-request.
        ignore_cache = False
        deadline = time.time() + self._timeout
        while True:

            # Acquire a SSO token if the service requires it by
            # calling the sso_exchange function.
            if self._sso_service and self._sso_exchange:
                kwargs['sso'] = self._sso_exchange(
                    self._sso_service, ignore_cache=ignore_cache)

            try:
                return self._do_single_request(
                    parsed_url,
                    targets,
                    request_encoder(kwargs),
                    headers,
                    response_decoder,
                    deadline,
                )
            except urllib3.exceptions.TimeoutError:
                # Rely on the deadline check in _do_single_request().
                pass
            except AuthenticationError:
                if ignore_cache:
                    raise
                ignore_cache = True
                # Roll back the increment on 'targets'

    def _request(self, path, **kwargs):
        return self._do_request(self._url + path, **kwargs)

    def _request_shard(self, path, shard, **kwargs):
        url = self._url % shard
        return self._do_request(url + path, **kwargs)

    def _method(self, url):
        def _method_wrapper(**kwargs):
            return self._request(url, **kwargs)
        return _method_wrapper

    def _sharded_method(self, url):
        def _method_wrapper(shard, **kwargs):
            return self._request_shard(url, shard, **kwargs)
        return _method_wrapper
