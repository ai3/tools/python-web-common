# Client stubs for common ai3 RPC services.

import codecs
from urllib.parse import urlencode
from .core import ClientStub


def _raw_request_encoder(req):
    return urlencode(req)


def _raw_response_decoder(resp):
    reader = codecs.getreader('utf-8')
    return reader(resp).read()


class LoginServiceClient(ClientStub):
    """Client for the SSO login service."""

    NAME = 'login'

    def exchange(self, **kwargs):
        # This is a normal HTTP POST request with urlencoded data.
        return self._do_request(
            self._url + '/exchange',
            headers={'Content-Type': 'application/x-www-form-urlencoded'},
            request_encoder=_raw_request_encoder,
            response_decoder=_raw_response_decoder,
            **kwargs,
        ).strip()


class AccountserverClient(ClientStub):
    """Client stub for the accountserver service."""

    NAME = 'accountserver'

    METHODS = [
        ('get_user', '/api/user/get'),
        ('search_users', '/api/user/search'),
        ('update_user', '/api/user/update'),
        ('disable_user', '/api/user/disable'),
        ('admin_update_user', '/api/user/admin_update'),
        ('reset_password', '/api/user/reset_password'),
        ('change_password', '/api/user/change_password'),
        ('set_account_recovery_hint', '/api/user/set_account_recovery_hint'),
        ('create_app_specific_password', '/api/user/create_app_specific_password'),
        ('delete_app_specific_password', '/api/user/delete_app_specific_password'),
        ('resource_reset_password', '/api/resource/reset_password'),
        ('disable_otp', '/api/user/disable_otp'),
        ('enable_otp', '/api/user/enable_otp'),
        ('disable_2fa', '/api/user/disable_2fa'),
        ('get_resource', '/api/resource/get'),
        ('set_resource_status', '/api/resource/set_status'),
        ('search_resources', '/api/resource/search'),
        ('email_add_alias', '/api/resource/email/add_alias'),
        ('email_delete_alias', '/api/resource/email/delete_alias'),
        ('email_set_openpgp_key', '/api/resource/email/set_openpgp_key'),
        ('web_set_php_version', '/api/resource/web/set_php_version'),
        ('move_resource', '/api/resource/move'),
        ('create_user', '/api/user/create'),
        ('create_resource', '/api/resource/create'),
        ('update_resource', '/api/resource/update'),
        ('recover_account', '/api/recover_account'),
        ('check_availability', '/api/resource/check_availability'),
    ]


class UserMetaDBClient(ClientStub):
    """Client for the usermetadb service."""

    NAME = 'umdb'

    METHODS = [
        ('get_user_devices', '/api/get_user_devices'),
        ('get_user_logs', '/api/get_user_logs'),
        ('get_last_login', '/api/get_last_login'),
    ]
