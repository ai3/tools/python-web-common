import mock
from unittest import TestCase

from ai_web_common.rpc import core


class TestDNS(TestCase):

    @mock.patch('ai_web_common.rpc.core.socket.getaddrinfo')
    def test_resolve_targets(self, gai):
        gai.return_value = [(0, 0, 0, 0, ('8.8.8.8', 443))]

        for i in range(2):
            result = core._Targets.resolve('dns.google.com', 443)
            self.assertEqual(result.next(),
                             ('8.8.8.8', 443))

        gai.assert_called_once_with('dns.google.com', 443)
