import json
from ai_web_common.rpc.test import RPCTestCase, BASE_FREE_PORT
from ai_web_common.rpc.core import BadRequestError, StatusError, \
    AuthenticationError


class TestRPC(RPCTestCase):

    def test_ok(self):
        errors = self.make_requests(self.client())
        self.assertEqual(0, errors)
        self.assertHasDecentDistribution()

    def test_nonexisting_targets(self):
        # Pick one 'good' server, and two 'bad' ones on ports
        # that won't be allocated. Expect all requests to succeed.
        errors = self.make_requests(self.client(),
                                    num_requests=10,
                                    targets=[
                                        ('127.0.0.1', BASE_FREE_PORT-2),
                                        ('127.0.0.1', BASE_FREE_PORT-1),
                                    ] + self.targets()[:1])
        self.assertEqual(0, errors)
        self.assertEqual(10, self.request_counters.get(0))


class TestSSL(RPCTestCase):

    SSL = True

    def test_ok_ssl(self):
        errors = self.make_requests(self.client())
        self.assertEqual(0, errors)
        self.assertHasDecentDistribution()


class TestOverload(RPCTestCase):

    def handle_request(self, resp, worker_idx, request_counter):
        # Make all servers return 429 except the first.
        self.send_json_response(
            resp, 429 if worker_idx > 0 else 200, '{}')

    def test_overloaded_targets(self):
        errors = self.make_requests(self.client(),
                                    num_requests=5)
        self.assertEqual(0, errors)
        self.assertEqual(5, self.request_counters.get(0))


class TestRequestError(RPCTestCase):

    NUM_SERVERS = 1
    ERR_DATA = {'field1': 'error1', 'field2': 'error2'}

    def handle_request(self, resp, worker_idx, request_counter):
        self.send_json_response(resp, 400, json.dumps(self.ERR_DATA))

    def test_bad_request_error(self):
        client = self.client()
        with self.assertRaises(BadRequestError) as exc:
            client.test_method(
                test_arg='42',
                override_target_list=self.targets())
        self.assertEqual(self.ERR_DATA, exc.exception.errors,
                         'StatusError.error does not match expectation')


class TestInternalServerError(RPCTestCase):

    NUM_SERVERS = 1

    def handle_request(self, resp, worker_idx, request_counter):
        self.send_response(resp, 500, 'oh no')

    def test_internal_server_error(self):
        client = self.client()
        with self.assertRaises(StatusError):
            client.test_method(
                test_arg='42',
                override_target_list=self.targets())


class TestTimeout(RPCTestCase):

    NUM_SERVERS = 1

    def test_timeout_error(self):
        client = self.client()
        with self.assertRaises(TimeoutError):
            client.test_method(
                test_arg='42',
                override_target_list=[('127.0.0.1', BASE_FREE_PORT-1)])


class TestSSO(RPCTestCase):

    NUM_SERVERS = 1

    def handle_request(self, resp, worker_idx, request_counter):
        content_length = int(resp.headers['Content-Length'])
        req = json.loads(resp.rfile.read(content_length))

        status = 200 if req.get('sso') == 'valid' else 403
        self.send_json_response(
            resp, status, '{}')

    def test_sso_exchange(self):
        tokens = ['expired', 'valid']
        def _exchange(service, ignore_cache):
            return tokens.pop(0)

        client = self.client(
            sso_exchange=_exchange,
            backend_extra={'sso_service': 'myservice/'},
        )
        client.test_method(
            test_arg='42',
            override_target_list=self.targets())

    def test_sso_fails_with_authentication_error(self):
        def _exchange(service, ignore_cache):
            return 'invalid'

        client = self.client(
            sso_exchange=_exchange,
            backend_extra={'sso_service': 'myservice/'},
        )
        with self.assertRaises(AuthenticationError):
            client.test_method(
                test_arg='42',
                override_target_list=self.targets())
