import datetime
import http.server
import json
import logging
import os
import shutil
import ssl
import tempfile
import threading
from unittest import TestCase

from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID

from ai_web_common.rpc import core


# Set a shorter timeout for tests.
core.DEFAULT_REQUEST_TIMEOUT = 3


def generate_ca():
    one_day = datetime.timedelta(days=1)
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048)

    certificate = x509.CertificateBuilder()\
        .subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, "test CA")]))\
        .issuer_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, "test CA")]))\
        .not_valid_before(datetime.datetime.today() - one_day)\
        .not_valid_after(datetime.datetime.today() + one_day)\
        .serial_number(x509.random_serial_number())\
        .public_key(private_key.public_key())\
        .add_extension(x509.BasicConstraints(ca=True, path_length=None),
                       critical=True)\
        .sign(private_key=private_key, algorithm=hashes.SHA256())
    return certificate, private_key


def generate_cert(ca_cert, ca_key, cn):
    one_day = datetime.timedelta(days=1)
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048)

    certificate = x509.CertificateBuilder()\
        .subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, cn)]))\
        .issuer_name(ca_cert.subject)\
        .not_valid_before(datetime.datetime.today() - one_day)\
        .not_valid_after(datetime.datetime.today() + one_day)\
        .serial_number(x509.random_serial_number())\
        .public_key(private_key.public_key())\
        .add_extension(x509.SubjectAlternativeName(
            [x509.DNSName(cn)],
        ), critical=False)\
        .add_extension(x509.BasicConstraints(ca=False, path_length=None),
                       critical=True)\
        .sign(private_key=ca_key, algorithm=hashes.SHA256())
    return certificate, private_key


class Counters():
    """Thread-safe request counter."""

    def __init__(self):
        self._lock = threading.Lock()
        self._data = {}

    def get(self, key):
        with self._lock:
            return self._data[key]

    def inc(self, key):
        with self._lock:
            if key in self._data:
                self._data[key] += 1
            else:
                self._data[key] = 1


class HTTPWorker(threading.Thread):
    """Worker thread for a standalone HTTP server."""

    def __init__(self, counters, key, port, ssl_context,
                 handle_fn):
        super(HTTPWorker, self).__init__(name='HTTP Worker')
        self._port = port
        self._ssl_context = ssl_context
        self._key = key
        self._stopped = threading.Event()

        request_counter = dict(value=0)
        request_counter_lock = threading.Lock()

        class _Handler(http.server.BaseHTTPRequestHandler):
            def do_POST(self):
                if not self.path.startswith('/'):
                    self.send_response(400)
                    self.end_headers()
                    return
                counters.inc(key)
                with request_counter_lock:
                    rc = request_counter['value']
                    request_counter['value'] += 1
                handle_fn(self, key, rc)

        self._handler = _Handler
        self._srv = None

    def run(self):
        self._srv = http.server.ThreadingHTTPServer(
            ('127.0.0.1', self._port),
            self._handler,
        )
        if self._ssl_context:
            self._srv.socket = self._ssl_context.wrap_socket(
                self._srv.socket, server_side=True)
        if self._stopped.is_set():
            return
        logging.info('started test http server %s', self._key)
        self._srv.serve_forever()

    def stop(self):
        self._stopped.set()
        if self._srv:
            logging.info('stopping test http server %s', self._key)
            self._srv.shutdown()
        self.join()


# Allocate free ports starting from BASE_FREE_PORT without ever
# repeating.
BASE_FREE_PORT = 10000
last_port = BASE_FREE_PORT - 1


def _free_port():
    global last_port
    last_port += 1
    return last_port


class MyClient(core.ClientStub):

    NAME = 'myclient'

    METHODS = [
        ('test_method', '/api/test/method'),
    ]


DEFAULT_NUM_REQUESTS = 100


class RPCTestCase(TestCase):

    SSL = False
    SERVER_NAME = 'my-service.localdomain'
    NUM_SERVERS = 5

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.request_counters = Counters()
        self.addrs = []
        self.servers = []
        self.server_ctx = None
        if self.SSL:
            self.server_ctx = self.init_ssl()
        for i in range(self.NUM_SERVERS):
            port = _free_port()
            s = HTTPWorker(
                self.request_counters, i, port,
                self.server_ctx,
                self.handle_request,
            )
            self.servers.append(s)
            self.addrs.append(('127.0.0.1', port))
            s.start()

    def tearDown(self):
        for s in self.servers:
            s.stop()
        shutil.rmtree(self.tmpdir, ignore_errors=True)

    def init_ssl(self):
        ca_cert, ca_key = generate_ca()
        client_cert, client_key = generate_cert(ca_cert, ca_key, 'client')
        server_cert, server_key = generate_cert(ca_cert, ca_key,
                                                self.SERVER_NAME)

        def _save(name, data):
            with open(os.path.join(self.tmpdir, name), 'wb') as f:
                f.write(data)

        _save('ca.pem', ca_cert.public_bytes(serialization.Encoding.PEM))
        _save('client.pem', client_cert.public_bytes(serialization.Encoding.PEM))
        _save('client.key', client_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.TraditionalOpenSSL,
            serialization.NoEncryption(),
        ))
        _save('server.pem', server_cert.public_bytes(serialization.Encoding.PEM))
        _save('server.key', server_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.TraditionalOpenSSL,
            serialization.NoEncryption(),
        ))

        ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ctx.verify_mode = ssl.CERT_REQUIRED
        ctx.check_hostname = False
        ctx.load_verify_locations(
            os.path.join(self.tmpdir, 'ca.pem'))
        ctx.load_cert_chain(
            os.path.join(self.tmpdir, 'server.pem'),
            os.path.join(self.tmpdir, 'server.key'))
        return ctx

    def send_response(self, handler, status, response_data):
        handler.send_response(status)
        handler.end_headers()
        handler.wfile.write(response_data.encode('utf-8'))

    def send_json_response(self, handler, status, response_data):
        handler.send_response(status)
        handler.send_header('Content-Type', 'application/json')
        handler.end_headers()
        handler.wfile.write(response_data.encode('utf-8'))

    # Handle a request, override in subclasses.
    def handle_request(self, resp, worker_idx, request_counter):
        self.send_json_response(resp, 200, '{}')

    def client(self, backend_extra={}, **kwargs):
        scheme = 'https' if self.SSL else 'http'
        backend = {'url': f'{scheme}://{self.SERVER_NAME}'}
        if self.SSL:
            backend['ssl_cert'] = os.path.join(self.tmpdir, 'client.pem')
            backend['ssl_key'] = os.path.join(self.tmpdir, 'client.key')
            backend['ssl_ca'] = os.path.join(self.tmpdir, 'ca.pem')
        backend.update(backend_extra)
        return MyClient(backend, **kwargs)

    def targets(self):
        return self.addrs[:]

    def make_requests(self, client, targets=None,
                      num_requests=DEFAULT_NUM_REQUESTS):
        if not targets:
            targets = self.targets()
        errors = 0
        for i in range(num_requests):
            try:
                client.test_method(
                    test_arg='42',
                    override_target_list=targets,
                )
            except Exception as e:
                logging.exception('http error: %s', e)
                errors += 1
        return errors

    def assertHasDecentDistribution(self,
                                    num_requests=DEFAULT_NUM_REQUESTS):
        # Assert that the requests have a 'decent' distribution over
        # the available backends.
        threshold = num_requests / (4 * self.NUM_SERVERS)
        for key in range(self.NUM_SERVERS):
            self.assertGreater(
                self.request_counters.get(key), threshold,
                f'counter for server {key} is lower than expected')
