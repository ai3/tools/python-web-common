from ai_web_common.rpc.test import RPCTestCase, BASE_FREE_PORT
from ai_web_common.rpc.clients import LoginServiceClient


class TestLoginClient(RPCTestCase):

    def handle_request(self, resp, worker_idx, request_counter):
        self.send_response(resp, 200, 'token')

    def test_login_exchange(self):
        client = LoginServiceClient({
            'url': f'http://{self.SERVER_NAME}',
        })
        resp = client.exchange(
            arg1='foo', arg2='bar',
            override_target_list=self.targets(),
        )
        self.assertEqual('token', resp)
