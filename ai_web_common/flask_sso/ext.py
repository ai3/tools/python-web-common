from functools import wraps
import binascii
import os
import sso
try:
    from urllib import urlencode
    import urlparse
except ImportError:
    from urllib.parse import urlencode
    import urllib.parse as urlparse

from flask import request, session, redirect, make_response, g, \
    render_template, abort, current_app
from flask_talisman import ALLOW_FROM

from opentelemetry import trace
from opentelemetry.semconv.trace import SpanAttributes


def init_sso(app, talisman):
    if 'SSO_LOGIN_SERVER' not in app.config:
        raise Exception('Must configure SSO_LOGIN_SERVER')
    if 'SSO_SERVICE' not in app.config:
        raise Exception('Must configure SSO_SERVICE')
    if 'SSO_DOMAIN' not in app.config:
        raise Exception('Must configure SSO_DOMAIN')

    # Allow passing the public key as a parameter (useful for testing)
    # or read it from a file.
    if 'SSO_PUBLIC_KEY' in app.config:
        pubkey = app.config['SSO_PUBLIC_KEY']
    else:
        pubkey_file = app.config.get(
            'SSO_PUBLIC_KEY_FILE', '/etc/sso/public.key')
        with open(pubkey_file, 'rb') as f:
            pubkey = f.read()

    # Ensure the login server URL is /-terminated.
    app.sso_login_server = app.config['SSO_LOGIN_SERVER'].rstrip('/') + '/'
    # Compute the CORS origin (drop the URL path if present).
    app.sso_login_server_origin = _get_origin(app.sso_login_server)
    app.sso_service = app.config['SSO_SERVICE']
    app.sso_cookie_name = app.config.get('SSO_COOKIE_NAME', '_sso')
    app.sso_validator = sso.Verifier(
        pubkey, app.config['SSO_SERVICE'], app.config['SSO_DOMAIN'],
        app.config.get('SSO_GROUPS'))
    if app.config.get('SSO_DEBUG'):
        app.logger.info(
            'SSO verifier created (service=%s, domain=%s, groups=%s)',
            app.config['SSO_SERVICE'], app.config['SSO_DOMAIN'],
            app.config.get('SSO_GROUPS'))

    # Install handlers for the SSO HTTP endpoints (sso_login and
    # sso_logout). We assume that the app is served at the domain root
    # (/), and attempt to derive the endpoints path prefix by looking
    # at the SSO_SERVICE, in case we need to set up authentication on
    # a subdirectory of the site only.
    service_path = app.sso_service[app.sso_service.find('/'):]
    app.add_url_rule(service_path + 'sso_login', 'sso_login', _sso_login)
    app.add_url_rule(service_path + 'sso_logout', 'sso_logout',
                     talisman(frame_options=ALLOW_FROM,
                              frame_options_alow_from='*')(_sso_logout))


def _get_origin(u):
    parsed = urlparse.urlsplit(u)
    return '%s://%s' % (parsed.scheme, parsed.netloc)


def _nonce():
    return binascii.hexlify(os.urandom(16)).decode()


def _redirect_to_login_server():
    # There isn't much point in sending a redirect on a POST request,
    # as the data will be lost anyway. Instead, explicitly fail and
    # tell the user to go back and retry (the browser might still have
    # the form data).
    if request.method == 'POST':
        return render_template('sso_expired_post.html')

    redir_data = {
        'n': session['nonce'],
        'd': request.url,
        's': current_app.sso_service,
    }
    if current_app.config.get('SSO_GROUPS'):
        redir_data['g'] = ','.join(current_app.config['SSO_GROUPS'])
    redir_url = current_app.sso_login_server + '?' + urlencode(redir_data)
    resp = make_response(redirect(redir_url))
    resp.set_cookie(current_app.sso_cookie_name, '',
                    expires=-1, secure=True, httponly=True)
    return resp


def auth_required(unauth_action=_redirect_to_login_server):
    """Wrap a handler with SSO authentication.

    This wrapper will redirect the user to the single sign-on login
    server if the request lacks a valid SSO ticket.

    Sets the 'g.current_user' variable to the name of the
    authenticated user, and 'g.sso_ticket' to the SSO ticket itself.

    """
    def _auth_required_decorator(func):
        @wraps(func)
        def _auth_wrapper(*args, **kwargs):
            if current_app.config.get('FAKE_SSO_USER'):
                g.current_user = current_app.config['FAKE_SSO_USER']
                g.sso_ticket = 'sso_ticket'
                g.raw_sso_ticket = 'sso_ticket'
                return func(*args, **kwargs)

            sso_ticket = request.cookies.get(current_app.sso_cookie_name)
            nonce = session.get('nonce')
            if not nonce:
                nonce = _nonce()
                session['nonce'] = nonce
            if not sso_ticket:
                return unauth_action()
            try:
                ticket = current_app.sso_validator.verify(sso_ticket, nonce)
                g.current_user = ticket.user()
                g.sso_ticket = ticket
                g.raw_sso_ticket = sso_ticket
            except sso.Error as e:
                # Intercept ACL errors due to group membership and return
                # an error instead of a redirect.
                #
                # Code -10 corresponds to SSO_ERR_NO_MATCHING_GROUPS.
                # if e.code == -10:
                # TODO: fix the ai/sso python error to include code again!
                if 'no matching groups' in str(e):
                    current_app.logger.error('authentication failed: %s', e)
                    abort(403)
                if current_app.config.get('SSO_DEBUG'):
                    errmsg = str(e)
                    errmsg += ' (ticket=%s, nonce=%s)' % (
                        sso_ticket.encode(),
                        nonce,
                    )
                    current_app.logger.exception(
                        'authentication failed: %s', errmsg)
                else:
                    current_app.logger.error('authentication failed: %s', e)
                return unauth_action()

            # Add authentication info to the current trace, if any.
            current_span = trace.get_current_span()
            if current_span:
                current_span.set_attribute(SpanAttributes.ENDUSER_ID, g.current_user)

            return func(*args, **kwargs)
        return _auth_wrapper
    return _auth_required_decorator


def _sso_login():
    if request.method != 'GET':
        current_app.logger.error('bad method on /sso_login')
        abort(405)
    if not request.args.get('t') or not request.args.get('d'):
        abort(400)

    resp = make_response(redirect(request.args['d']))
    resp.set_cookie(current_app.sso_cookie_name, request.args['t'],
                    secure=True, httponly=True)
    return resp


def _sso_logout():
    if request.method != 'GET':
        abort(405)

    # Clear everything from the current session.
    session.clear()

    resp = make_response('OK')
    resp.headers['Access-Control-Allow-Origin'] = \
        current_app.sso_login_server_origin
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    resp.set_cookie(current_app.sso_cookie_name, '',
                    expires=-1, secure=True, httponly=True)
    return resp
