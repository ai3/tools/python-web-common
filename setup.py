#!/usr/bin/python

from setuptools import setup, find_packages


setup(
    name="ai-web-common",
    version="3.0",
    description="A/I Web Application Common Code",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/tools/python-web-common",
    install_requires=[
        "backoff",
        "Flask",
        "flask-talisman",
        "opentelemetry-distro",
        "opentelemetry-exporter-zipkin-json",
        "opentelemetry-instrumentation-flask",
        "sso",
        "urllib3",
        "whitenoise",
    ],
    zip_safe=False,
    packages=find_packages(),
    entry_points={},
)
