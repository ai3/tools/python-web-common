ai_web_common
====

This Python package contains helpers and utilities to run user-facing
web applications safely. It is assumed that the web apps will be based
on [Flask](https://flask.palletprojects.com). This package is meant to
reduce the amount of copy&pasted code between our various web
applications.

### Features

* SSO authentication (in-app, not via Apache module or
  sso-proxy)
* CSP and other common security-related HTTP headers
* Serve pre-compressed assets transparently
* Helpers to manage user language selection, for multi-language
  applications
* Helpers for making RPC calls to internal services
* Tracing, via OpenCensus and Zipkin




